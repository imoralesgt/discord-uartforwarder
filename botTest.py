import discord
import dotenv
import os

client = discord.Client()
dotenv.load_dotenv('token.env')



@client.event
async def on_ready():
    print("Logged in as {0.user}".format(client))
    
@client.event
async def on_message(message):
    if message.author == client.user:
        return
    
    if message.content.startswith('$uart'):
        await message.channel.send('UART forwarding back to you!')

client.run(os.getenv('DISCORD_TOKEN'))